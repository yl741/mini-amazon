# mini-amazon

Group Name: Mahjong.   
Group Member: Bokai Zhou, Yuxin Lu, Ruiqi Zhang, Zixuan Zheng.  
  
Mission: Choose the standard project task to build a mini Amazon, to enhance understanding and implementation of databases and git management. 
  
Assignment:  
● Users Guru: responsible for Account / Purchases, Bokai Zhou.  
● Products Guru: responsible for Products / Cart, Yuxin Lu.  
● Sellers Guru: responsible for Inventory / Order Fulfillment, Ruiqi Zhang.  
● Social Guru: responsible for Feedback / Messaging, Zixuan Zheng.  
